<?php

declare(strict_types=1);

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class ORM
{
	protected static $_instance;

	protected $_entity_manager;
	protected static $_paths = [];
	protected static $_encryptor;

	protected function __construct()
	{
		$isDevMode = false;

		// the connection configuration
		$dbParams = \Config::get('db');

		$config = Setup::createAnnotationMetadataConfiguration(static::$_paths, $isDevMode, null, null, false);
		$this->_entity_manager = EntityManager::create($dbParams, $config);

		$secret = pack("H*", \Config::get('db')['secret']);

		\Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
			'DoctrineEncrypt\Configuration\Encrypted', 
			DOCROOT."/vendor/51systems/doctrine-encrypt/src"
		);

		static::$_encryptor = new \DoctrineEncrypt\Encryptors\AES256Encryptor($secret);

		$subscriber = new \DoctrineEncrypt\Subscribers\DoctrineEncryptSubscriber(
			new \Doctrine\Common\Annotations\AnnotationReader,
			static::$_encryptor
		);

		$this->_entity_manager->getEventManager()->addEventSubscriber($subscriber);
		$this->_addSaveEvent();
	}

	public static function instance()
	{
		!static::$_instance and static::$_instance = new static();

		return static::$_instance;
	}

	public function getEntityManager()
	{
		return $this->_entity_manager;
	}

	public static function addEntityPath(string $path)
	{
		if(static::$_instance){
			throw new \Exception("Paths must be added before ORM is initialized");
		}

		static::$_paths[] = $path;
	}

	public static function encryptor()
	{
		return static::$_encryptor;
	}

	public function _addSaveEvent()
	{
		$entity_manager = $this->getEntityManager();
		register_shutdown_function(function() use($entity_manager){
			if($entity_manager){
				try{
					$entity_manager->flush();
					return true;
				}
				catch(\Exception $e){
					\Log::error($e->getMessage());
				}
			}
		});
	}
}