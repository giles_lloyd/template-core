<?php

class Asset{
	protected static $_css = [];
	protected static $_js = [];

	public static function add_css($file){
		static::$_css[] = $file;
	}

	public static function add_js($file){
		static::$_js[] = $file;
	}

	public static function css(){
		return static::$_css;
	}

	public static function js(){
		return static::$_js;
	}
}