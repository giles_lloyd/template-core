<?php
declare(strict_types=1);

class Controller
{
	protected $_di_container;
	protected $_status;

	function __construct()
	{
		$this->_di_container = \App::container();
	}

	public function setStatus(int $code)
	{
		$this->_status = $code;
	}

	public function getStatus()
	{
		return $this->_status ?: HttpStatus::OK;
	}
}