<?php

declare(strict_types=1);

class Request
{
	protected static $_instance;

	public static function instance() : Psr\Http\Message\ServerRequestInterface
	{
		return static::$_instance;
	}

	public static function register(Psr\Http\Message\ServerRequestInterface $request)
	{
		static::$_instance = $request;
	}

	public static function getPUTData()
	{
		parse_str(file_get_contents("php://input"), $post_vars);
		return $post_vars;
	}

	public static function isCli() : bool
	{
		return (php_sapi_name() === 'cli');
	}
}