<?php

class Autoloader
{
	protected static $_instance;

	public static function instance()
	{
		if(!static::$_instance){
			static::$_instance = require("vendor/autoload.php");
		}

		return static::$_instance;
	}
}

return Autoloader::instance();