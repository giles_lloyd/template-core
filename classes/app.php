<?php
class App
{
	protected static $_di_container;

	public static function view($file, $params = [])
	{
		if(($pkg = static::get_controller_namespace()) && (is_dir("packages/".$pkg."/views"))){
			$path = "packages/".$pkg."/views/";
		}
		elseif($dir = static::get_view_directory()){
			$path = $dir;
		}
		else{
			$path = "views/";
		}

		if(is_file($path.$file)){
			extract($params);
			ob_start();
			require($path.$file);
			$view = ob_get_contents();
			ob_end_clean();
			return $view;
		}
		else{
			die("View: '".$path.$file."' Does not exist");
		}
	}

	public static function render($content)
	{
		return \Theme::instance()->renderTemplate((string)$content);
	}

	public static function get_controller_namespace(){
		$trace = debug_backtrace();

		$controller = false;

		foreach($trace as $step){
			 if(isset($step['class']) && ($step['class'] != "App") && (strpos($step['class'], "Controller") !== false)){
				$controller = $step['class'];
			 }
		}

		if($controller){
			$reflector = new ReflectionCLass($controller);
			return strtolower($reflector->getNamespaceName());
		}

		return false;
	}

	public static function get_view_directory(){
		$trace = debug_backtrace();

		$dir = false;
		foreach($trace as $step){
			 if(isset($step['file']) && (strpos($step['file'], "/views/") !== false)){
				$path_segments = [];
				foreach(explode("/", $step['file']) as $segment){
					$path_segments[] = $segment;
					if($segment == "views"){
						break;
					}
				}

				return implode("/", $path_segments)."/";
			 }
		}

		return false;
	}

	public static function redirect($url){
		header('Location: '.$url);
		exit;
	}

	public static function process_async($url, $params){
		ignore_user_abort();

		foreach ($params as $key => &$val) {
		if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);

		$url = "http://localhost".$url;
		$parts=parse_url($url);

		$fp = fsockopen($parts['host'],
			isset($parts['port'])?$parts['port']:80,
			$errno, $errstr, 30);

		$out = "POST ".$parts['path']." HTTP/1.1\r\n";
		$out.= "Host: ".$parts['host']."\r\n";
		$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
		$out.= "Content-Length: ".strlen($post_string)."\r\n";
		$out.= "Connection: Close\r\n\r\n";
		if (isset($post_string)) $out.= $post_string;

		fwrite($fp, $out);
		fclose($fp);
	}

	public static function load_packages(){
		if(is_dir(PACKAGEPATH)){
			$ignore = ['.', '..'];
			foreach(scandir(PACKAGEPATH, SCANDIR_SORT_ASCENDING) as $package){
				if(in_array($package, $ignore)){
					continue;
				}

				static::load_package($package);
			}
		}
	}

	public static function load_package(string $package)
	{
		$package = "/".$package;
		if(is_dir(PACKAGEPATH.$package)){
			if(file_exists(PACKAGEPATH.$package.'/bootstrap.php')){
				require_once(PACKAGEPATH.$package.'/bootstrap.php');
			}
		}
	}

	public static function container()
	{
		!static::$_di_container and static::$_di_container = new \League\Container\Container();

		return static::$_di_container;
	}
}
?>