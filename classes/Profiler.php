<?php
use Psr\Http\Message\RequestInterface as Request;

class Profiler
{
	public static function forge()
	{
		return new static();
	}

	public function run(Request $request){
		if(array_key_exists("RUN_PROFILE", $request->getQueryParams()) || isset($_SESSION['enable_profiling'])){
			$_SESSION['enable_profiling'] = true;
			tideways_enable(TIDEWAYS_FLAGS_NO_SPANS);

			register_shutdown_function(function(){
				$data = tideways_disable();
				$profiling_config = \Config::get('profiling');
				if(isset($profiling_config['display_name'])){
					$name = $profiling_config['display_name'];
				}
				else{
					$name = "no_name_assigned";
				}

				file_put_contents(
					sprintf("%s/%s.%s.xhprof", sys_get_temp_dir(), uniqid(), $name),
					serialize($data)
				);
			});
		}
	}
}