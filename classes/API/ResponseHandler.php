<?php
namespace API;

use Exception\HttpForbidden;

class ResponseHandler
{
	protected $_bad_response_codes = [400, 500, 403, 410, 405, 204, 406, 404, 503, 401, 415];

	public function run($response, $http_code)
	{
		if(in_array($http_code, $this->_bad_response_codes)){
			$response = json_decode($response, true);
			if($http_code === 403){
				throw new HttpForbidden($response['body']);
			}
			if(isset($response['body'])){
				throw new \Exception\APIException($response['body'], $http_code);
			}
			elseif(isset($response['error'])){
				throw new \Exception\APIException($response['error'], $http_code);
			}
			else{
				\Log::error("Some error occurred, here is the response: ".json_encode($response));
				throw new \Exception\APIException("An unknown error occurred", $http_code);
			}
		}

		return $response;
	}
}