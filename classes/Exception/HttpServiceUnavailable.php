<?php
namespace Exception;

class HttpServiceUnavailable extends HttpError
{
	public function __construct($message = "Service Unavailable", $code = 503, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}