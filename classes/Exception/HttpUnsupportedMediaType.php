<?php
namespace Exception;

class HttpUnsupportedMediaType extends HttpError
{
	public function __construct($message = "We cannot process your input. Please provide in the correct format (JSON)", $code = 415, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}