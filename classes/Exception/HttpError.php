<?php
namespace Exception;

class HttpError extends \Exception
{
	public function __construct($message = "Internal Server Error", $code = 500, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}