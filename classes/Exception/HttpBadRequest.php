<?php
namespace Exception;

class HttpBadRequest extends HttpError
{
	public function __construct($message = "Bad Request", $code = 400, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}