<?php
namespace Exception;

class APIException extends \Exception
{
	public function __construct($message = "An unknown error occurred", $code = 0, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}