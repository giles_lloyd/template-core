<?php
namespace Exception;

class HttpMethodNotAllowed extends HttpError
{
	public function __construct($message = "Method Not Allowed", $code = 405, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}