<?php
namespace Exception;

class HttpForbidden extends HttpError
{
	public function __construct($message = "Forbidden", $code = 403, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}