<?php
namespace Exception;

class HttpNotAcceptable extends HttpError
{
	public function __construct($message = "We currently only serve JSON or XML", $code = 406, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}