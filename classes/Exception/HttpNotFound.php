<?php
namespace Exception;

class HttpNotFound extends \Exception
{
	public function __construct($message = "Page Not Found", $code = 404, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}