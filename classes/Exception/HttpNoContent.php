<?php
namespace Exception;

class HttpNoContent extends HttpError
{
	public function __construct($message = "No Content", $code = 204, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}