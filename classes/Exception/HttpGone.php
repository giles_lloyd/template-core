<?php
namespace Exception;

class HttpGone extends HttpError
{
	public function __construct($message = "Gone", $code = 410, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}