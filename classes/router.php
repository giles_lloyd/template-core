<?php

class Router{
	public static function forge(){
		return new static();
	}

	public function get_route(){
		$url = parse_url(urldecode($_SERVER['REQUEST_URI']))['path'];
		$url = rtrim($url, '/');

		if($route = $this->match_route($url, $this->get_routes())){
			return $route;
		}

		if($route = $this->match_partial_route($url, $this->get_routes())){
			return $route;
		}

		return $url;
	}

	protected function get_routes(){
		return \Config::get("routes") ?: [];
	}

	protected function match_route($url, array $routes){
		foreach($routes as $path => $route){
			if($url == $path){
				return $route;
			}
			elseif((strpos($path, "(:any)") !== false) && (strpos($url, str_replace("(:any)", "", $path)) === 0)){
				return str_replace(
					"(:params)",
					str_replace(
						str_replace("(:any)", "", $path),
						"",
						$url
					),
					$route
				);
			}
		}

		return false;
	}

	protected function match_partial_route($url, array $routes){
		$partial_url = '';

		foreach(explode('/', $url) as $segment){
			if($segment){
				$partial_url .= '/'.$segment;
				foreach($routes as $path => $route){
					if($partial_url."/*" == $path){
						return $route.str_replace($partial_url, '', $url);
					}
				}
			}
		}

		return false;
	}

	public static function route($route)
	{
		$segments = explode('/', $route);
		foreach($segments as $key => &$segment){
			if($segment == ""){
				unset($segments[$key]);
			}
		}
		$segments = array_values($segments);
		$urls = Config::get('urls');

		foreach($segments as $key => &$segment){
			if(in_array($segment, $urls['ignore'])){
				unset($segments[$key]);
			}
		}
		$segments = array_values($segments);

		if(count($segments) == 0){
			if(class_exists("Controller\\Index")){
				$class = new Controller\Index;
			}
		}
		else{
			$class_name = !in_array('controller', $segments) ? 'Controller' : '';
			foreach($segments as $number => $part){
				$class_name .= "\\".ucfirst($part);
				if(class_exists($class_name)){
					$class = new $class_name;

					$remaining_segments = [];
					foreach($segments as $count => $seg){
						if($count > $number){
							$remaining_segments[] = $seg;
						}
					}

					break;
				}
			}

			isset($remaining_segments) and $segments = $remaining_segments;
		}

		if(!isset($class)){
			throw new Exception\HttpNotFound();
		}

		$segments = array_values($segments);
		if(count($segments) > 0){
			if(isset($segments[1]) && method_exists($class, $segments[1])){
				$method = $segments[1];
				unset($segments[1]);
			}
			else if(method_exists($class, $segments[0])){
				$method = $segments[0];
				unset($segments[0]);
			}
			else if(method_exists($class, strtolower(\Request::instance()->getMethod()))){
				$method = strtolower(\Request::instance()->getMethod());
			}
			elseif(method_exists($class, 'index')){
				$method = 'index';
			}
			else{
				throw new Exception\HttpNotFound();
			}
		}
		else{
			foreach([strtolower(\Request::instance()->getMethod()), 'index'] as $potential_method){
				if(method_exists($class, $potential_method)){
					$method = $potential_method;
					break;
				}
			}

			if(!isset($method)){
				throw new Exception\HttpNotFound();
			}
		}
		if(count($segments) > 0){
			$params = $segments;
		}
		else{
			$params = array();
		}
		return array('class' => $class, 'method' => $method, 'params' => $params);
	}

	protected static function _buildClassName($segments, $num)
	{
		$first = ucfirst(reset($segments));
		$class_name = sprintf("\\%s\\Controller", $first);

		for($i = 0; $i < $num; $i++){
			$class_name .= "\\".ucfirst(next($segments));
		}

		return $class_name;
	}
}