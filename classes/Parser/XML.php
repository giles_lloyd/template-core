<?php

declare(strict_types=1);

namespace Parser;

class XML
{
	protected static $_instance;

	private function __construct(){}

	public static function instance()
	{
		!static::$_instance and static::$_instance = new static();

		return static::$_instance;
	}

	public function fromArray(array $data, &$xml_data = null)
	{
		!$xml_data and $xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

		foreach( $data as $key => $value ) {
			if( is_array($value) ) {
				if( is_numeric($key) ){
					$key = 'item'.$key;
				}
				$subnode = $xml_data->addChild($key);
				$this->fromArray($value, $subnode);
			} else {
				$xml_data->addChild("$key",htmlspecialchars("$value"));
			}
		}

		return $xml_data;
	}
}