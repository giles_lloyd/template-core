<?php
namespace Middleware;

use Exception\HttpForbidden;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class Router
{
	protected static $_app;

	public function __invoke(Request $request, Response $response, callable $next)
	{
		$content_type_headers = $response->getHeader("content-type");

		$router = \Router::forge();
		$uri = $router->get_route();

		try{
			$route = $router->route($uri);
			$output = $this->getOutput($route);
			$response = $this->setHttpStatus($response);
		}
		catch(\Exception\HttpNotFound $e){
			$static_view = sprintf("%s/views%s.php", DOCROOT, $uri);
			if(file_exists($static_view)){
				$output = \Theme::instance()->renderView($static_view);
			}
			else{
				$content = $this->_404(reset($content_type_headers));
				$response = $response->withStatus(404, "Page Not Found");
			}
		}
		catch(\Exception\HttpError $e){
			if ($e instanceof HttpForbidden) {
				throw $e;
			}

			$response = $response->withStatus($e->getCode());
			$content = $this->getContent($e->getMessage(), reset($content_type_headers));
		}

		if(!isset($content)){
			$content = $this->getContent($output, reset($content_type_headers));
		}

		$response->getBody()->write($content);
	    $response = $next($request, $response);

		return $response;
	}

	protected function _404(string $content_type)
	{
		if(strpos($content_type, "text/html") !== false){
			return \Theme::instance()->renderView(DOCROOT."/views/errors/404.php");
		}

		$body = ['body' => 'Page not Found'];
		if(strpos($content_type, "application/json") !== false){
			return json_encode($body);
		}
	}

	protected function getOutput($route)
	{
		static::$_app = $route['class'];
		define('CONTROLLER', get_class(static::$_app));
		define('METHOD', $route['method']);
		if(method_exists(static::$_app, 'before')){
			static::$_app->before();
		}
		if($route['params']){
			$output = call_user_func_array(array(static::$_app, $route['method']), $route['params']);
		}
		else{
			$method = $route['method'];
			$output = static::$_app->$method();
		}

		if($route['method'] === "post"){
			static::$_app->setStatus(\HttpStatus::CREATED);
		}

		return $output;
	}

	protected function setHttpStatus(Response $response)
	{
		if(method_exists(static::$_app, "getStatus")){
			return $response->withStatus(static::$_app->getStatus());
		}

		return $response;
	}

	protected function getContent($output, string $content_type)
	{
		if(strpos($content_type, "text/html") !== false){
			return \App::render($output);
		}
		if(strpos($content_type, "application/json") !== false){
			is_string($output) and $output = ['body' => $output];
			return json_encode($output);
		}

		return $output;
	}
}