<?php

class Environment
{
	const DEVELOPMENT = 'development';

	const STAGING = 'staging';

	const PRODUCTION = 'production';
}
