<?php
class Log{
	protected static function _Log($type, $message){
		try{
			if(defined("CONTROLLER") && defined("METHOD")){
				$message = sprintf("%s %s %s(%s): %s", date("g:i:sa"), $type, CONTROLLER, METHOD, $message);
			}
			else{
				$message = sprintf("%s %s: %s", date("g:i:sa"), $type, $message);
			}

			$dir = sprintf("logs/%s/%s", date('Y'), date('m'));
			if(!is_dir($dir)){
				umask(0);
				mkdir($dir, 0777, true);
			}

			$file = $dir."/".date('d').".txt";
			
			file_put_contents($file, $message."\n", FILE_APPEND);
		}
		catch(\Exception $e){}
	}

	public static function info($message){
		static::_Log("INFO", $message);
	}

	public static function error($message){
		static::_Log("ERROR", $message);
	}
}