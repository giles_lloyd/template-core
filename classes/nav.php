<?php
class Nav{
	public static $_items = array(
		"Home" => "/",
	);

	public static function add_items(array $items){
		static::$_items = array_merge(static::$_items, $items);
	}

	public static function render(){
		$menu_items = Nav::$_items;
		$view = App::view("nav.php", array('menu_items' => $menu_items));
		return $view;
	}
}
?>