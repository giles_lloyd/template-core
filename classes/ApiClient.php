<?php

class ApiClient{
	protected $base_url;
	protected $type = "GET";
	protected $request_url;
	protected $header = [];
	protected $params;
	protected $_response_handler;

	function __construct($base){
		$this->base_url = $base;
		$this->_response_handler = new \API\ResponseHandler();
	}

	public static function open($base){
		$apiclient = new static($base);
		return $apiclient;
	}

	public function request_type($type){
		$this->type = $type;

		return $this;
	}

	public function request($url){
		$this->request_url = $url;

		return $this;
	}

	public function add_header($header){
		$this->header[] = $header;

		return $this;
	}

	public function set_params($params){
		$this->params = $params;

		return $this;
	}

	public function execute(){
		$url = $this->base_url . $this->request_url;

		\Log::info("Making API request to: {$url}");

		if(!$this->header){
			return file_get_contents($url);
		}

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		if($this->type == "POST"){
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $this->_getFlattenedParams() ?: '');
		}
		elseif($this->type == "PUT"){
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->type);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->params));
		}
		elseif($this->type != 'GET'){
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->type);
        }

		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->header);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		return $this->_response_handler->run($response, $httpcode);
	}

	protected function _getFlattenedParams()
	{
		$flattened_array = [];

		foreach($this->params as $key => $val){
			if(is_array($val)){
				foreach($this->_flattenArray($key, $val) as $flattened){
					$flattened_array[$flattened['key']] = $flattened['val'];
				}
			}
			else{
				$flattened_array[$key] = $val;
			}
		}

		return$flattened_array;
	}

	protected function _flattenArray($parent_key, array $input_array)
	{
		foreach($input_array as $key => $val){
			$flat_key = sprintf("%s[%s]", $parent_key, $key);
			if(is_array($val)){
				foreach($this->_flattenArray($flat_key, $val) as $flattened){
					yield ['key' => $flattened['key'], 'val' => $flattened['val']];
				}
			}
			else{
				yield ['key' => $flat_key, 'val' => $val];
			}
		}
	}
}