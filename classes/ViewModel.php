<?php

abstract class ViewModel
{
	protected $_view;
	protected $_di_container;

	function __construct(array $data = [])
	{
		foreach($data as $prop => $val){
			$this->{$prop} = $val;
		}

		$this->_di_container = \App::container();
	}

	function __toString()
	{
		$this->view();
		return \App::view($this->_view, get_object_vars($this));
	}

	public function view(){}
}