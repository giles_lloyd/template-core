<?php
class DB{
	protected static $_connection;

	public static function query($query){
		$result = static::mysqli()->query($query);

		if($result !== true){
			return $result;
		}
		else{
			return static::mysqli()->insert_id;
		}
	}

	protected static function mysqli(){
		if(!static::$_connection){
			require(DOCROOT.'/resources/dbConnect.php');
			static::$_connection = $mysqli;
		}

		return static::$_connection;
	}

	public static function escape($text){
		return static::mysqli()->real_escape_string($text);
	}
}