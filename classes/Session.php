<?php
class Session{
	public static function set_flash($type, $message){
		$_SESSION['flash'][$type] = $message;
	}

	public static function read_flash(){
		require("views/session/flash_message.php");
	}

	public static function get_flash($type)
	{
		$response = null;

		if (isset($_SESSION['flash'][$type])) {
			$response = $_SESSION['flash'][$type];
			unset($_SESSION['flash'][$type]);
		}

		return $response;
	}
	
	public static function setup(){
		if(Config::get('session')['enabled']){
			session_start();
		}
	}
}
