<?php

declare(strict_types=1);

class Config
{
	protected static $_env;

	/**
	 * Return the environment specified in the root
	 * .env file. Return 'production' as default
	 * @return string
	 */
	public static function getEnv() : string
	{
		!static::$_env and static::$_env = static::_readEnvFile();

		return static::$_env ?: "production";
	}

	protected static function _readEnvFile()
	{
		if(file_exists(DOCROOT."/.env")){
			return trim(file_get_contents(DOCROOT."/.env"));
		}

		return false;
	}

	public static function get($config) : array
	{
		$configs = [];
		$env_specific_path = sprintf("%s/config/%s/%s.php", DOCROOT, static::getEnv(), $config);
		$default_path = sprintf("%s/config/%s.php", DOCROOT, $config);

		if(file_exists($env_specific_path)){
			return require($env_specific_path);
		}
		else if(file_exists($default_path)){
			return require($default_path);
		}

		return [];
	}
}
?>