<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;

class Middleware
{
	protected static $_queue = [];

	public static function add($middleware)
	{
		static::$_queue[] = $middleware;
	}

	public static function getQueue()
	{
		return static::$_queue;
	}

	public static function run(Request $request, Response $response) : Response
	{
		$relayBuilder = new Relay\RelayBuilder();
		$relay = $relayBuilder->newInstance(static::$_queue);
		$response = $relay($request, $response);

		return $response;
	}
}