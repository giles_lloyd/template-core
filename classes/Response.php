<?php

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Response
{
	public static function createFromRequest(RequestInterface $request) : ResponseInterface
	{
		$header = current($request->getHeader("accept"));

		if(strpos($header, "html") !== false){
			return new Zend\Diactoros\Response\HtmlResponse("");
		}

		if(strpos($header, "xml") !== false){
			return new Zend\Diactoros\Response\TextResponse("");
		}

		if(strpos($header, "json") !== false){
			return new Zend\Diactoros\Response\JsonResponse("");
		}

		return new Zend\Diactoros\Response\HtmlResponse("");
	}

	public static function create401(RequestInterface $request, $error_message) : ResponseInterface
	{
		$response = static::createFromRequest($request);
		$response = $response->withStatus(401, "Unauthorized");

		$content = $error_message;
		if($response instanceof Zend\Diactoros\Response\HtmlResponse){
			$content = \Theme::instance()->renderView(DOCROOT."/views/errors/401.php", ['error' => $error_message]);
		}
		elseif($response instanceof Zend\Diactoros\Response\JsonResponse){
			$content = json_encode(['body' => $error_message]);
		}

		$response->getBody()->write($content);

		return $response;
	}

	public static function create403(RequestInterface $request, $error_message) : ResponseInterface
	{
		$response = static::createFromRequest($request);
		$response = $response->withStatus(403, "Forbidden");

		$content = $error_message;
		if($response instanceof Zend\Diactoros\Response\HtmlResponse){
			$content = \Theme::instance()->renderView("views/errors/403.php", ['error' => $error_message]);
		}
		elseif($response instanceof Zend\Diactoros\Response\JsonResponse){
			$content = json_encode(['body' => $error_message]);
		}

		$response->getBody()->write($content);

		return $response;
	}
}