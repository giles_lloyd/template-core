<?php

class Theme
{
	protected static $_instance;
	protected $_template;

	protected $_js = [];
	protected $_css = [];

	private function __construct(){
	    $this->_template = new \LondonEdgeUI\ViewModel\TemplateViewModel();
    }

	public static function instance()
	{
		!static::$_instance and static::$_instance = new static();

		return static::$_instance;
	}

	public function setTemplate(string $template)
	{
		$this->_template = $template;
	}

	public function getTemplate()
	{
		return $this->_template;
	}

	public function addJs(string $js)
	{
		$this->_js[] = $js;
	}

	public function addCss(string $css)
	{
		$this->_css[] = $css;
	}

	public function js()
	{
		return $this->_js;
	}

	public function css()
	{
		return $this->_css;
	}

	public function renderTemplate($content)
    {
        $this->_template->setContent($content);

        return $this->_template;
    }

	public function renderView($view, $params = [])
	{
		extract($params);
		ob_start();
		require($view);
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}
}