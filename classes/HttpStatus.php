<?php

class HttpStatus
{
	const OK = 200;
	const CREATED = 201;
	const ACCEPTED = 202;
}