<?php

class Model
{
	function __construct(array $info = [])
	{
		foreach ($info as $property => $value) {
			if(property_exists($this, $property)){
				$this->{$property} = $value;
			}
		}

		$this->afterForge();
	}
	public static function forge(array $info = []){
		$model = new static($info);

		return $model;
	}

	public function update(array $data){
		foreach($data as $prop => $val){
			if(property_exists($this, $prop)){
				$this->$prop = $val;
			}
		}
	}

	public function to_array()
	{
		return $this->toArray();
	}

	public function toArray()
	{
		$data = get_object_vars($this);
		foreach($data as $prop => $val){
			if((is_array($this->{$prop}) || ($this->{$prop} instanceof \Traversable))){
				$new_val = [];
				foreach($this->{$prop} as $array_key => $array_val){
					if(is_object($array_val) && method_exists($array_val, "toArray")){
						$new_val[$array_key] = $array_val->toArray();
					}
				}

				count($new_val) and $data[$prop] = $new_val;
			}
		}

		return $data;
	}

	public function afterForge()
	{
		if(property_exists($this, "created_at") && !$this->created_at){
			$this->created_at = new \DateTimeImmutable();
		}
	}
}
