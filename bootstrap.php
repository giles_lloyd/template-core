<?php

define("PACKAGEPATH", DOCROOT.'/packages');

$autoloader = require("classes/autoloader.php");

Session::setup();
App::load_packages();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
	$_SERVER,
	$_GET,
	$_POST,
	$_COOKIE,
	$_FILES
);


Profiler::forge()->run($request);
Request::register($request);
$response = Response::createFromRequest($request);

require("bootstrap.php");

if(!Request::isCli()){
	Middleware::add(new Middleware\Router());

	try{
		$response = Middleware::run($request, $response);
	}
	catch(\Exception\HttpUnauthorized $e){
		$response = Response::create401($request, $e->getMessage());
	}
	catch(\Exception\HttpForbidden $e){
		$response = Response::create403($request, $e->getMessage());
	}

	$server = new Zend\Diactoros\Server(
		function ($request, $response, $done) {
		},
		$request,
		$response
	);

	$server->listen(function ($request, $response, $error = null) {
		if (! $error) {
			return;
		}
		
		var_dump($error); die("handle this in core bootstrap");
	});
}